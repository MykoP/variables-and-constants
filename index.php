<?php

$variable = 'test';
$integer = '123';

$vAR = 'Igor';
$vaR = 'Mykola';

echo "$vAR", "$vaR";
echo '<br>';

$nullValue = null;
if (is_null($nullValue)) {
    echo "null value check";
    echo '<br>';
}

$object = (object)array();
if (is_object($object)) {
    echo 'object value check';
    echo '<br>';
}

$string = "$nullValue String and null";
if (is_string($string)) {
    echo 'string value check';
    echo '<br>';
}

$number = '123';
if (is_numeric($number)) {
    if (!is_int($number)) {
        echo 'no, it is not integer 2';
        echo '<br>';
    } else {
        echo 'yes, it is number 2';
        echo '<br>';
    }

    $number = 123.123;
    if (is_numeric($number)) {
        echo 'yes, it is float 3';
        echo '<br>';
    } else {
        echo 'yes, it is number 3';
        echo '<br>';
    }
}

$bool = false;
echo($bool ? 'true' : 'false');
echo '<br>';
echo '<br>';


var_dump(gettype($nullValue));
echo '<br>';
var_dump(gettype($string));
echo '<br>';
var_dump(gettype($object));
echo '<br>';
var_dump(gettype($number));
echo '<br>';
var_dump(gettype($bool));
echo '<br>';

settype($nullValue, 'string');
echo '<br>';
settype($number, 'int');
echo '<br>';

if (!isset($other)) {
    echo 'non found';
    echo '<br>';
} else {
    settype($other, 'integer');
    echo $other;
    echo '<br>';
}

$variable = 10; // Глобальна зона видимості

function localVariableCheck()
{
    global $variable; // Оголошення глобальної змінної
    echo $variable; // Посилання на локальну змінну
}

localVariableCheck();
echo '<br>';

function globalCheck()
{
    echo $GLOBALS['variable'];
}

globalCheck();
echo '<br>';

$numOne = 100;
$numTwo = 200;
$sum = 0;

function sum()
{
    global $numOne, $numTwo, $sum;
    $sum = $numOne + $numTwo;
}

sum();
echo $sum;
echo '<br>';


function summerGlobal()
{
    $GLOBALS['sum'] = $GLOBALS['numOne'] + $GLOBALS['numTwo'];
}

summerGlobal();
echo $sum;
echo '<br>';

//var_dump($_SERVER);
//var_dump($GLOBALS);
//var_dump($_GET);
//var_dump($_FILES);
//var_dump($_COOKIE);
//var_dump($_SESSION);
//var_dump($_REQUEST);
